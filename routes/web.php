<?php

use App\Http\Controllers\DailyMotion\DailyMotionSearch;
use App\Http\Controllers\EnergyBill\EnergyBillController;
use App\Http\Controllers\EnergyBillCalculator\BillCalculator;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/bill',[EnergyBillController::class,'index']);
Route::post('/bill/store',[EnergyBillController::class,'store'])->name('bill.store');





