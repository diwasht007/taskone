<?php

use App\Http\Controllers\DailyMotion\DailyMotionController;
use App\Http\Controllers\EnergyBillCalculate\Billcalculator;
use App\Http\Controllers\K5cHolding\DataFetchingController;
use App\Http\Controllers\WordCounter\WordCounter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('dailymotion',[DailyMotionController::class,'search']);
Route::get('getmglink',[DataFetchingController::class,'getMgLinkData']);
Route::get('counter',[WordCounter::class,'getWordCount']);



