<?php

namespace App\Http\Controllers\K5cHolding;

use App\Http\Controllers\Controller;
use App\Models\Symbol;
use App\Models\Trading;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class DataFetchingController extends Controller
{
    public function getMgLinkData()
    {
       
        $currentTime = Carbon::now()->format('H');
        $colsingTime = Carbon::createFromTimeString('07:00')->format('H');
        
        
        $token = self::login();
        $indiceslivedata = self::getAllCompaniesLiveData($token);
        try{
            
            foreach ($indiceslivedata as $data) {

                $symbol = Symbol::updateorcreate([
                    'symbol'=>$data['Symbol'],
                    'description'=>$data['CompanyName'],
                ],
                [
                    'symbol'=>$data['Symbol'],
                    'description'=>$data['CompanyName'],
                ]);
                
                $trading = Trading::updateorcreate([
                    'high'=>$data['High'],
                    'low'=>$data['Low'],
                    'close'=>$data['Close'],
                    'volume'=>$data['Volume'],
                    'open'=>$currentTime == $colsingTime? $data['Close']:null,

                    
                ],
                [
                    'high'=>$data['High'],
                    'low'=>$data['Low'],
                    'close'=>$data['Close'],
                    'volume'=>$data['Volume'],
                    'symbol_id'=> $symbol->id,
                    'open'=>$currentTime == $colsingTime? $data['Close']:null,

    
                ]);
            }
            }catch(Exception $e){
                return response()->json(['error'=>$e->getMessage()],500);

            }
        }
    
    private static function login()
    {
        $response = Http::asForm()->post('https://api.mg-link.net/api/auth/token', [
            'grant_type' => 'password',
            'username' => 'K5AUS@2022',
            'password' => 'K5@2022!'
        ]);
        $data = json_decode($response->body(), true);
        $token = ($data['access_token']);
        return $token;
    }
    private static function getindicesLiveData($token)
    {
        $response = Http::withHeaders([
            'Authorization' => 'Bearer ' . $token
        ])->get('https://api.mg-link.net/api/Data1/GetPSXIndicesLive');
        $data = json_decode($response->body(), true);
        return response()->json($data);
    }
    private static function getAllCompaniesLiveData($token)
    {
        $response = Http::withHeaders([
            'Authorization' => 'Bearer ' . $token
        ])->get('https://api.mg-link.net/api/Data1/GetPSXAllCompaniesLive');

        return json_decode($response->body(), true);
    }
}

