<?php

namespace App\Http\Controllers\WordCounter;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;


class WordCounter extends Controller
{
    const SIZE = 26;
    public function getWordCount(Request $request){
        $str = $request->str;
       return self::countWord($str);

    }
    // function to print the character and its frequency in order of its occurrence
    private static function countWord($str){
        $n =strlen($str);
        $freq = array_fill(0, self::SIZE, NULL);
        $result = [];
        for($i=0; $i<$n; $i++)
            $freq[ord($str[$i])- ord('a')]++;
        for($i = 0; $i<$n; $i++){
            if($freq[ord($str[$i]) - ord('a')] !=0){
                $result[] =  $str[$i].":". $freq[ord($str[$i])-ord('a')]." ";
                $freq[ord($str[$i])-ord('a')] = 0;
            }
        }
        //array to string
        return implode(",",$result);



    }
    
}



