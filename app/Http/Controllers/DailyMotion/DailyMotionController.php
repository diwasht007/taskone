<?php

namespace App\Http\Controllers\DailyMotion;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;

class DailyMotionController extends Controller
{
    public function search(Request $request)
    {

        $user_input = $request->channel;
        $allowedChannel = array('lifestyle', 'auto', 'videogames', 'music', 'news');
        if( preg_match('('.implode('|',$allowedChannel).')', $user_input))
                return $this->dailySearch($request->search, $user_input);
             else {
                return response()->json(['error' => 'please enter channel lifestyle, auto, videogames, music or news']);
            }
        }
    
    public function dailySearch(string $searchString, string $channelName)
    {
        $response = Http::get('https://api.dailymotion.com/videos', [
            'channel' => $channelName,
            'fields' => 'id,title,owner,owner.screenname,owner.url,channel',
            'search' => $searchString,
            'limit' => 12
        ]);
        $data = json_decode($response->body(), true);
        return response()->json($data);
    }
}
