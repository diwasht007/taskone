@extends('layouts.master')
@section('content')

<form action="{{route('bill.store')}}" method="POST">
    @csrf
   
    <div class="index">
        <div class="form-group">
            <div class="col-md-4">
                <input id="units" name="units" placeholder="Please enter no. of Units" class="form-control input-md"
                    type="text">
                    <H3>The total bill amount is {{isset($bill)?$bill:0}}</H3>
            </div>
        </div>
        <!-- Button -->
        <div class="form-group">
            <div class="col-md-4">
                <button id="singlebutton" name="unit-submit" class="btn btn-primary">Submit</button>

            
            </div>
        </div>
    </div>
</form>

@endsection